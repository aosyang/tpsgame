// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TPSGame : ModuleRules
{
	public TPSGame(ReadOnlyTargetRules TargetRules)
        : base(TargetRules)
	{
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "GameplayDebugger" });
	}
}
