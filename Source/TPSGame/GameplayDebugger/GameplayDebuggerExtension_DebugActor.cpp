
#include "TPSGame.h"
#include "TPSGame/GameplayDebugger/GameplayDebuggerExtension_DebugActor.h"
#include "GameplayDebuggerCategoryReplicator.h"
#include "EngineUtils.h"
#include "GameFramework/Character.h"

#if WITH_GAMEPLAY_DEBUGGER

FGameplayDebuggerExtension_DebugActor::FGameplayDebuggerExtension_DebugActor()
{
	BindKeyPress(TEXT("LeftBracket"),	this, &FGameplayDebuggerExtension_DebugActor::OnSelectNextDebugActor);
	BindKeyPress(TEXT("RightBracket"),	this, &FGameplayDebuggerExtension_DebugActor::OnSelectPrevDebugActor);
}

TSharedRef<FGameplayDebuggerExtension> FGameplayDebuggerExtension_DebugActor::MakeInstance()
{
	return MakeShareable(new FGameplayDebuggerExtension_DebugActor());
}

void FGameplayDebuggerExtension_DebugActor::OnSelectNextDebugActor()
{
	CycleThroughDebugActors(1);
}

void FGameplayDebuggerExtension_DebugActor::OnSelectPrevDebugActor()
{
	CycleThroughDebugActors(-1);
}

void FGameplayDebuggerExtension_DebugActor::CycleThroughDebugActors(int32 Offset)
{
	AGameplayDebuggerCategoryReplicator* Replicator = GetReplicator();
	check(Replicator);

	const AActor* DebugActor = Replicator->GetDebugActor();
	const UWorld* World = Replicator->GetWorld();
	check(World);

	TArray<ACharacter*> CharacterList;
	int32 NumPawns = World->GetNumPawns();

	for (auto PawnItr = World->GetPawnIterator(); PawnItr; ++PawnItr)
	{
		CharacterList.Add(CastChecked<ACharacter>(*PawnItr));
	}

	int32 DebugActorIndex = CharacterList.IndexOfByPredicate([&DebugActor](const ACharacter* Character)
	{
		return Character == DebugActor;
	});

	if (DebugActorIndex != INDEX_NONE)
	{
		Replicator->SetDebugActor(CharacterList[(DebugActorIndex + Offset + NumPawns) % NumPawns]);
		return;
	}

	Replicator->SetDebugActor(NumPawns ? CharacterList[0] : nullptr);
}

#endif // WITH_GAMEPLAY_DEBUGGER
