#pragma once

#include "GameplayDebuggerExtension.h"

#if WITH_GAMEPLAY_DEBUGGER

class FGameplayDebuggerExtension_DebugActor : public FGameplayDebuggerExtension
{
public:
	FGameplayDebuggerExtension_DebugActor();

	static TSharedRef<FGameplayDebuggerExtension> MakeInstance();

protected:
	void OnSelectNextDebugActor();
	void OnSelectPrevDebugActor();

	void CycleThroughDebugActors(int32 Offset);
};

#endif // WITH_GAMEPLAY_DEBUGGER
