#pragma once

#include "GameFramework/PawnMovementComponent.h"
#include "PhysicsPawnMovement.generated.h"

UCLASS(ClassGroup = Movement, meta = (BlueprintSpawnableComponent))
class UPhysicsPawnMovement : public UPawnMovementComponent
{
	GENERATED_UCLASS_BODY()

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PhysicsPawnMovement)
	float TorqueFactor;

private:
	UStaticMeshComponent*	PhysicsMesh;
};
