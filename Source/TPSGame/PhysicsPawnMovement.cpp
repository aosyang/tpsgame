

#include "TPSGame.h"

#include "PhysicsPawnMovement.h"

UPhysicsPawnMovement::UPhysicsPawnMovement(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

void UPhysicsPawnMovement::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (PhysicsMesh == nullptr)
	{
		const AActor* Owner = GetOwner();

		if (Owner != nullptr)
		{
			PhysicsMesh = Owner->FindComponentByClass<UStaticMeshComponent>();
		}
	}

	if (PhysicsMesh != nullptr)
	{
		const AController* Controller = PawnOwner->GetController();
		if (Controller && Controller->IsLocalController())
		{
			if (Controller->IsFollowingAPath())
			{
				const FVector PlannarAcceleration = Velocity.GetSafeNormal2D();
				const FVector Torque = FVector::CrossProduct(FVector(0, 0, 1), PlannarAcceleration);

				//PhysicsMesh->AddImpulse(PlannarAcceleration * 10.0f, NAME_None, true);
				PhysicsMesh->SetAllPhysicsAngularVelocity(Torque * TorqueFactor);

				const FVector ActorLoc = GetOwner()->GetActorLocation();
				DrawDebugLine(GetWorld(), ActorLoc, ActorLoc + PlannarAcceleration * 100.0f, FColor::Green, false, -1.0f, 0, 2.0f);
			}
		}
	}
}
