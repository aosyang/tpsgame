// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "TPSGame.h"
#include "TPSGameMode.h"
#include "TPSGameCharacter.h"
#include "GameplayDebugger.h"
#include "GameplayDebugger/GameplayDebuggerExtension_DebugActor.h"

ATPSGameMode::ATPSGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	IGameplayDebugger::Get().RegisterExtension(TEXT("DebugActor"), IGameplayDebugger::FOnGetExtension::CreateStatic(&FGameplayDebuggerExtension_DebugActor::MakeInstance));
	IGameplayDebugger::Get().NotifyExtensionsChanged();
}
